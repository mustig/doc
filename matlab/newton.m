clc

N = 2;

w = sym('w', [N, 1]);

f(w) = 1/2 *  w.' * w
g(w) = [w.' * w]

l = sym('l', [size(g,1), 1]);

df = jacobian(f, w).';
dg = jacobian(g, w).';

L = f + l.'*g;
%dL = df - dg*l;
H = hessian(L, w)

KKT = [H dg; dg.' 0]

KKT_num = matlabFunction(KKT, 'Vars', {w, l});
df_num = matlabFunction(df, 'Vars', {w});
g_num = matlabFunction(g, 'Vars', {w});

w_step = ones(size(w));
l_step = ones(size(l));

% Begin looping here
    newtonstep = - KKT_num(w_step, l_step)\[df_num(w_step); g_num(w_step)];
    w_step = w_step + newtonstep(1:N)
    l_step = newtonstep(N+1:end)