## Direct Optimal Control assignments

### Requirements

+ Python 2.7
+ CasADi
+ Common. sense?

Tested and developed on Lubuntu 15.04/15.10.

### Author
Simon Pedersen  
Department of Signals and Systems  
Chalmers University of Technology  
[pesimon@chalmers.se](mailto:pesimon@chalmers.se)
