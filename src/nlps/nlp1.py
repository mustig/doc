import numpy as np
import casadi as cs

def cost(w):
    f = 0.5*cs.mul(w.T, w)
    return f

def eqConstr(w):
    g = cs.veccat([
        sum(w)-5,
        w[0]
    ])
    return g
