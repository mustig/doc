import numpy as np
import casadi as cs

def cost(w):
    f = 0.5*cs.mul(w.T, w)
    return f

def eqConstr(w):
    g = cs.veccat([
        w[0]**2 - 2*w[1]**3 - w[1] - 10*w[2],
        w[1] + 10*w[2]
    ])
    return g
