import numpy as np
import casadi as cs

def cost(w):
    f = 0.5*cs.mul(w.T, w) + sum(w)
    return f

def eqConstr(w):
    g = cs.veccat([
        cs.mul(w.T, w) - 1,
        w[0]
    ])
    return g
