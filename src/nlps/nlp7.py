import numpy as np
import casadi as cs

def cost(w):
    f = 0.5*cs.mul(w.T, w) + sum(w)
    return f

def eqConstr(w):
    g = cs.veccat([
        cs.mul(w.T, w) - 1
    ])
    return g

def inEqConstr(w):
    h = cs.veccat([
      -w[0]**2 - w[1] + 0.5
    ])
    return h
