import numpy as np
import casadi as cs

def armijo(w, T, jac_T, alpha, beta):
  def lineSearchFunc(_w, _dw):
    t = 1.0
    _T = T(_w)
    _jac_T = alpha*cs.mul(jac_T(_w), _dw)
    while t > 1e-20:
      LHS = T(_w + t*_dw)
      RHS = _T + t*_jac_T
      if LHS < RHS:
        break
      t = beta*t
    return t
  return lineSearchFunc


# Here's more witchcraft.
def makeT1(p, jac_p, g, jac_g, nu):
  T1 = lambda w: p([w])[0] + nu*np.linalg.norm(g([w])[0], 1)
  jac_T1 = lambda w: jac_p([w])[0] + nu*cs.mul(np.sign(g([w])[0]).T, jac_g([w])[0]) # Size 1 x n
  return (T1, jac_T1)

def makeT1withInEq(p, jac_p, g, jac_g, h, jac_h, nu):
  T1 = lambda w: p([w])[0] + nu*np.linalg.norm(g([w])[0], 1)
  def jac_T1(w):
    P = jac_p([w])[0]
    _jac_g, _g = jac_g([w])
    G = nu*cs.mul(np.sign(_g).T, _jac_g)
    _jac_h, _h = jac_h([w])
    H = nu*cs.mul(_h > np.zeros_like(_h), _jac_h)
    return P+G+H
  return (T1, jac_T1)
