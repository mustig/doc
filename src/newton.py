import numpy as np
import casadi as cs

def solve(w, l, H, g, linesearch, w0 = None, l0 = None, tol = 1e-8, maxIterations=500, gaussNewton = False):
  # If gaussNewton = False, H = Hessian
  # If True, H = Jacobian

  nl = l.size()

  if w0 != None: # Unpythonic but necessary
    _w = w0
  else:
    _w = np.ones((w.size(), 1))
  if l0 != None:
    _l = l0
  else:
    _l = np.zeros((nl, 1))

  zFill = cs.Sparsity(nl, nl)

  report = {}
  report['exitConditions'] = []
  report['stepSize'] = []
  report['trajectories'] = [_w]

  for i in range(maxIterations-1):
    # Calculate (Hessian), L-gradient and constraint gradient.
    if gaussNewton:
        _jac_L, _ =  H([_w, _l]) # function H here is actually jacobian
        _grad_L = _jac_L.T
        _H = approximateHessian(_grad_L)
    else:
        _H, _grad_L, _ = H([_w, _l])

    _jac_g, _g = g([_w])

    # Check tolerances now that we have L-gradient and g
    ex = np.linalg.norm(cs.veccat([_grad_L, _g]), 1)
    report["exitConditions"].append(ex)
    if ex < tol:
      break

    # Solve for a newton step
    dw, dl = newtonStep(_w, _l, _grad_L, _H, _g, _jac_g.T, zFill)

    # Deploy the chosen line search algorithm (passed as a parameter)
    t = linesearch(_w, dw)
    report['stepSize'].append(t)

    # Update position
    _w = _w + t*dw
    _l = _l + t*dl

    report['trajectories'].append(_w)

  report["iterations"] = i

  return (_w, _l, report)


def newtonStep(w, l, grad_L, H, g, grad_g, zFill):
  KKT = cs.blockcat([[H, grad_g], [grad_g.T, zFill]])
  RHS = -cs.vertcat([grad_L, g])

  step = np.linalg.solve(KKT, RHS)

  nw = w.shape[0]
  dw, dl = step[0:nw], step[nw:] # Slice result into w-step and lambda-step.
  return (dw, dl)

def approximateHessian(grad_L, a = 0):
    # Approximate the hessian with the Gauss-Newton scheme
    return np.identity(grad_L.shape[0])
