import numpy as np
import matplotlib.pyplot as plt
import casadi as cas

def spam(w, l, r, m = None):
  np.set_printoptions(precision=3, suppress=True)
  print('Optimum:\n %s' % w.T)
  print('Lagrange multiplier(s):\n %s' % l.T)
  if m:
    print('Inequality Lagrange multiplier(s):\n %s' % m.T)
  print('Number of iterations: %d' % r['iterations'])

def plotTraj(wlist, axLim = 1.2):
  f = plt.subplot(2,2,1)
  plt.plot(wlist[:, 0], wlist[:, 1], 'go-')
  th = np.linspace(0, np.pi*2.0, 100)
  plt.plot(np.cos(th), np.sin(th), 'b--')
  plt.axis([-1*axLim, axLim, -1*axLim, axLim])
  plt.title(r'Trajectory of $w_1$ and $w_2$')
  plt.xlabel(r'$w_1$')
  plt.ylabel(r'$w_2$')
  plt.gca().set_aspect('equal')
  plt.draw()
  return f

def plotEx(ex):
  f = plt.subplot(2,2,2)
  plt.plot(ex, 'ro-')
  tol = np.ones_like(ex)*1e-8
  plt.plot(tol, 'k--')
  plt.yscale('log')
  plt.title(r'Convergence')
  plt.xlabel(r'Iteration')
  plt.ylabel(r'$\| \|\ \nabla \mathcal{L}\quad g\ \| \|_1 $')
  plt.draw()
  return f

def plotStepSize(t):
  f = plt.subplot(2,2,3)
  plt.plot(t, 'ko-')
  plt.title(r'Step size')
  plt.xlabel(r'Iteration')
  plt.ylabel(r'$t$')
  plt.draw()
  plt.show()
  return f

def savePlots(d, figs, prefix):
  for k, f in figs.iteritems(): # Py 2.7 only
    plt.figure(f.number)
    plt.savefig('/home/pesimon/workspace/doc/output/figures/%s/%s_%s.png' % (d, k, prefix))
