import numpy as np
import casadi as cs
import newton

def solve(w, l, jac_p, H, g, linesearch, w0 = None, l0 = None, tol = 1e-8, maxIterations=500, gaussNewton = False):
  # If gaussNewton = False, H = Hessian
  # If True, H = Jacobian

  nl = l.size()

  if w0 != None: # Unpythonic but necessary
    _w = w0
  else:
    _w = np.ones((w.size(), 1))
  if l0 != None:
    _l = l0
  else:
    _l = np.zeros((nl, 1))

  zFill = cs.Sparsity(nl, nl)

  report = {}
  report['exitConditions'] = []
  report['stepSize'] = []
  report['trajectories'] = [_w]

  for i in range(maxIterations-1):
    # Calculate (Hessian), L-gradient and constraint gradient.
    if gaussNewton:
        _jac_L, _ =  H([_w, _l]) # function H here is actually jacobian
        _grad_L = _jac_L.T
        _H = newton.approximateHessian(_grad_L)
    else:
        _H, _grad_L, _ = H([_w, _l])

    _jac_g, _g = g([_w])
    _jac_p, _p = jac_p([_w])

    # Check tolerances now that we have L-gradient and g
    ex = np.linalg.norm(cs.veccat([_grad_L, _g]), 1)
    report["exitConditions"].append(ex)
    if ex < tol:
      break

    # Solve for a newton step
    dw, lplus, _ = QPStep(_w, _l, _p, _jac_p, _H, _g, _jac_g)
 
    # Deploy the chosen line search algorithm (passed as a parameter)
    t = linesearch(_w, dw)
    report['stepSize'].append(t)

    # Update position
    _w = _w + t*dw
    _l = (1-t)*_l + t*lplus

    report['trajectories'].append(_w)

  report["iterations"] = i

  return (_w, _l, report)

def QPStep(w, l, p, jac_p, H, g, jac_g):
  W = cs.MX.sym("w", w.shape)

  qpCost = lambda: cs.MX(0.5*cs.mul([W.T, H, W]) + cs.mul([jac_p, W]))
  qpConstraints = lambda: (cs.MX(g) + cs.mul(cs.MX(jac_g), W))

  QP = cs.MXFunction('QP', cs.nlpIn(x=W), cs.nlpOut(f = qpCost(), g = qpConstraints()))

  S = cs.NlpSolver("S", "ipopt", QP, {"print_level": 0, "print_time": False})
  arg = dict()
  arg["x0"] = [1, 1]
  arg["lbx"] = [-cs.inf, -cs.inf]
  arg["ubx"] = [cs.inf, cs.inf]
  arg["lbg"] = cs.veccat([np.zeros_like(g)])
  arg["ubg"] = cs.veccat([np.zeros_like(g)])

  res = S(arg)
  return (np.array(res['x']), np.array(res['lam_g']), res)


def solveInEq(w, l, m, jac_p, H, g, h, linesearch, w0 = None, l0 = None, m0 = None, tol = 1e-8, maxIterations=500):
  # If gaussNewton = False, H = Hessian
  # If True, H = Jacobian

  nl = l.size()
  nm = m.size()

  if w0 != None: # Unpythonic but necessary
    _w = w0
  else:
    _w = np.ones((w.size(), 1))
  if l0 != None:
    _l = l0
  else:
    _l = np.zeros((nl, 1))
  if m0 != None:
    _m = m0
  else:
    _m = np.zeros((nm, 1))

  zFill = cs.Sparsity(nl, nl)

  report = {}
  report['exitConditions'] = []
  report['stepSize'] = []
  report['trajectories'] = [_w]

  for i in range(maxIterations-1):
    # Calculate (Hessian), L-gradient and constraint gradient.
    _H, _grad_L, _ = H([_w, _l, _m])

    _jac_g, _g = g([_w])
    _jac_h, _h = h([_w])
    _jac_p, _p = jac_p([_w])

    # Check tolerances now that we have L-gradient and g
    ex = np.linalg.norm(cs.veccat([_grad_L, _g]), 1)
    report["exitConditions"].append(ex)
    if ex < tol:
      break

    # Solve for a newton step
    dw, lplus, mplus, _ = QPStepInEq(_w, _l, _m, _p, _jac_p, _H, _g, _jac_g, _h, _jac_h)

    # Deploy the chosen line search algorithm (passed as a parameter)
    t = linesearch(_w, dw)
    report['stepSize'].append(t)

    # Update position
    _w = _w + t*dw
    _l = (1-t)*_l + t*lplus
    _m = (1-t)*_m + t*mplus

    report['trajectories'].append(_w)

  report["iterations"] = i

  return (_w, _l, _m, report)

def QPStepInEq(w, l, m, p, jac_p, H, g, jac_g, h, jac_h):
  W = cs.MX.sym("w", w.shape)

  qpCost = lambda: cs.MX(0.5*cs.mul([W.T, H, W]) + cs.mul([jac_p, W]))
  qpConstraints = lambda: (cs.MX(cs.vertcat([g, h])) + cs.mul(cs.MX(cs.vertcat([jac_g, jac_h])), cs.vertcat([W])))

  QP = cs.MXFunction('QP', cs.nlpIn(x=W), cs.nlpOut(f = qpCost(), g = qpConstraints()))

  S = cs.NlpSolver("S", "ipopt", QP, {"print_level": 0, "print_time": False})
  arg = dict()
  arg["x0"] = [1, 1]
  arg["lbx"] = [-cs.inf, -cs.inf]
  arg["ubx"] = [cs.inf, cs.inf]
  nl = g.shape[0]
  nm = h.shape[0]
  arg["lbg"] = cs.veccat([np.zeros_like(g), [-cs.inf]*nm])
  arg["ubg"] = cs.veccat([np.zeros_like(g), [0]*nm])

  res = S(arg)
  dw = np.array(res['x'])
  lmplus = np.array(res['lam_g'])
  return (dw, lmplus[0:nl], lmplus[nl:], res)
