#!/usr/bin/env python
import sys
sys.path.append('/opt/casadi')
import numpy as np
#import matplotlib.pyplot as plt
import casadi as cs

# Choose a NLP to solve
import nlps.nlp4 as nlp
# ---------------------

import myioutils as mio
import kkt
import linesearch
import newton
import sqp

def main():
  n = 2 # Dimensions in w so that w in R^n
  w = cs.SX.sym('w', n) # w is (n x 1) symbolic.

  # Create the dual variables and equations/functions needed
  l, p, jac_p, g, jac_g, L, jac_L, H = kkt.dual(w, nlp.cost, nlp.eqConstr)

  # Prepare a line search function using Armijo's backtracking algorithm on a T1 merit function
  T1, jac_T1 = linesearch.makeT1(p, jac_p, g, jac_g, 1.0)
  lineSearchFunc = linesearch.armijo(w, T1, jac_T1, 0.5, 0.5)

  w0 = np.array([[1], [1]])
  l0 = np.array([[1]])


  print('\nExact hessian')
  try:
    opt_w, opt_lambda, report = sqp.solve(w, l, jac_p, H, jac_g, lineSearchFunc, w0 = w0, l0 = l0)
  except np.linalg.linalg.LinAlgError as e:
    if 'Singular matrix' in e.message:
      print("KKT matrix is singular!")
  else:
    fig = dict()
    fig['traj'] = mio.plotTraj(np.array(report['trajectories']), axLim = 2.0)
    fig['ex'] = mio.plotEx(np.array(report['exitConditions']))
    fig['t'] = mio.plotStepSize(np.array(report['stepSize']))
#    mio.savePlots('ass2', fig, 'p4d3')
    mio.spam(opt_w, opt_lambda, report)

if __name__ == "__main__":
  main()
