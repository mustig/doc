import numpy as np
import casadi as cs

def dual(w, cost, eqConstr):
  _g = eqConstr(w)
  p = cs.SXFunction('p', [w], [cost(w)])
  g = cs.SXFunction('g', [w], [_g])

  l = cs.SX.sym('l', _g.size()) # As many lambdas as there are equality constraints

  def lagrangian(w, l, p, g):
    return p([w])[0] + cs.mul(l.T, g([w])[0]) # The slicing here? Witchcraft, I say.

  L = cs.SXFunction('L', [w, l], [lagrangian(w, l, p, g)])

  jac_p = p.jacobian(0, 0)
  jac_g = g.jacobian(0, 0)
  H = L.hessian(0, 0)
  jac_L = L.jacobian(0, 0)

  return (l, p, jac_p, g, jac_g, L, jac_L, H)

def dual_ineq(w, cost, eqConstr, inEqConstr):
  _g = eqConstr(w)
  _h = inEqConstr(w)
  p = cs.SXFunction('p', [w], [cost(w)])
  g = cs.SXFunction('g', [w], [_g])
  h = cs.SXFunction('h', [w], [_h])

  l = cs.SX.sym('l', _g.size()) # As many lambdas as there are equality constraints
  m = cs.SX.sym('m', _h.size()) # As many mu:s as there are inequality constraints

  def lagrangian(w, l, m, p, g, h):
    return p([w])[0] + cs.mul(l.T, g([w])[0]) + cs.mul(m.T, h([w])[0])# The slicing here? Witchcraft, I say.

  L = cs.SXFunction('L', [w, l, m], [lagrangian(w, l, m, p, g, h)])

  jac_p = p.jacobian(0, 0)
  jac_g = g.jacobian(0, 0)
  jac_h = h.jacobian(0, 0)
  H = L.hessian(0, 0)
  jac_L = L.jacobian(0, 0)

  return (l, m, p, jac_p, g, jac_g, h, jac_h, L, jac_L, H)
